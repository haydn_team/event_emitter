Event Emitter
=======================

Javascript style event emitter module.

----

### Installation

Only tested on Python 3.X

From source:

```sn
$ python setup.py install
```

For production:

```sn
$ pip install event-emitter
```

### Usage

```python
from haydn.event_emitter import EventEmitter


def callback_with_parameter(p1, p2):
    print('test event callback, p1 = {}, p2 = {}'.format(p1, p2))


def callback():
    print('test event callback')


ee = EventEmitter()
ee.on('event', callback)
ee.on('event_with_param', callback_with_parameter)


ee.fire('event')
ee.fire('event_with_param', 'event_parameter1', 'event_parameter2')
```


