from distutils.core import setup

setup(
    name='event-emitter',
    packages=['haydn.event_emitter'],
    version='0.3.0',
    description='Javascript style event emitter for Python3',
    author='Jason Lee',
    url='https://bitbucket.org/haydn_team/event_emitter/',
    download_url='https://bitbucket.org/haydn_team/event_emitter.git',
    license='MIT',
    author_email='leemin0830@gmail.com'
)
